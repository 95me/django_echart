
from django.shortcuts import render
from django.shortcuts import HttpResponse
import json

def barSimple(request):

    return render(request,'echart/echart_bar/bar_simple.html')

def barSimpleData(request):

    dataAxis = ['运动鞋','帽子','外套','牛仔裤','内衣','秋裤','衬衫']

    data = [120,200,150,80,70,110,130]
    legend=['销量']

    datadict={'dataAxis':dataAxis,'data':data,'legend':legend}
    bardata=json.dumps(datadict)
    print(bardata)
    return HttpResponse(bardata,content_type='application/json', charset='utf-8')