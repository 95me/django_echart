from django.shortcuts import render
from django.shortcuts import HttpResponse

# Create your views here.


def index(request):
    return HttpResponse("this is echart index")


def echartFirst(request):
    return render(request,'echart/first.html')

# echarts数据更新实例
def echartUpdate(request):
    return render(request, 'echart/echart_update_data.html')

def echartDataset(request):
    return render(request,'echart/echart_dataset.html')

def echartMedia(request):
    return render(request,'echart/echart_media.html')

def echartVisualMap(request):
    return render(request,'echart/echart_visualMap.html')

def echartEvent(request):
    return render(request,'echart/echart_event.html')

def echartGL(request):
    return render(request,'echart/echart_GL.html')

def echartCustom(request):
    return render(request,'echart/echart_custom.html')