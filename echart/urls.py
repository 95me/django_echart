
from django.urls import path
from . import views
from .bar import barview

app_name='echart'
urlpatterns=[
    path('',views.index,name='index'),
    path('first',views.echartFirst,name='first'),
    path('echartupdate',views.echartUpdate,name='echartupdate'),
    path('echartdataset',views.echartDataset,name='echartdataset'),
    path('barsimple',barview.barSimple,name='barsimple'),
    path('barsimpledata',barview.barSimpleData,name='barsimpledata'),
    path('echartmedia',views.echartMedia,name='echartmedia'),
    path('echartvisualmap',views.echartVisualMap,name='echartvisualmap'),
    path('echartevent',views.echartEvent,name='echartevent'),
    path('echartgl',views.echartGL,name='echartgl'),
    path('echartcustom',views.echartCustom,name='echartcustom'),
]
